#!/bin/bash

check_command "mysqldump"
check_command "mysql"

function stash_prepare_db {
    info "Prepared backup of DB ${STASH_DB} in ${STASH_BACKUP_DB}"
}

function stash_backup_db {
    rm -r ${STASH_BACKUP_DB}
    mkdir -p ${STASH_BACKUP_DB}
    mysqldump -u ${STASH_DB_USER} -p${STASH_DB_PASS} --complete-insert --opt --skip-lock-tables --single-transaction ${STASH_DB} > ${STASH_BACKUP_DB}/stash.mysql
    if [ $? != 0 ]; then
        bail "Unable to backup ${STASH_DB} to ${STASH_BACKUP_DB}"
    fi
    info "Performed backup of DB ${STASH_DB} in ${STASH_BACKUP_DB}"
}

function stash_bail_if_db_exists {
#TODO: figure out if we need this :D
info todo
}

function stash_restore_db {
    cat ${STASH_RESTORE_DB}/stash.mysql |mysql -u ${STASH_DB_USER} -p${STASH_DB_PASS} ${STASH_DB}
    if [ $? != 0 ]; then
        bail "Unable to restore ${STASH_RESTORE_DB} to ${STASH_DB}"
    fi
    info "Performed restore of ${STASH_RESTORE_DB} to DB ${STASH_DB}"
}